'use strict';
const Strings = require(`${__dirname}/../config/Strings`);
const Constants = require(`${__dirname}/../config/Constants`);

class TabletopM {

    constructor(s_max_x, s_max_y) {
        this.n_max_x = parseInt(s_max_x);
        this.n_max_y = parseInt(s_max_y);
        this.a_marked_coords = [];
    }

    static check_coordinates(n_x, n_y) {
        let o_resp = { b_valid: false, s_msg: Strings.coordinates_error };

        if (!n_x || !n_y || n_x == null || n_y == null) {
            return o_resp;
        }

        const b_all_numbers = [n_x, n_y].every((s_coord) => {
            return !isNaN(parseInt(s_coord.trim()));
        });

        const b_all_non_decimal = [n_x, n_y].every((s_coord) => {
            return parseInt(s_coord) % 1 == 0;
        });

        const b_every_ok = [n_x, n_y].every((n_coord) => {
            return parseInt(n_coord) > 0 && parseInt(n_coord) <= 50;
        });

        if (b_every_ok && b_all_non_decimal && b_all_numbers) {
            o_resp = { b_valid: true };
        }

        return o_resp;
    }

    check_coords_in_tt(n_x, n_y) {
        const b_x = 0 <= n_x && n_x <= this.n_max_x;
        const b_y = 0 <= n_y && n_y <= this.n_max_y;

        let o_resp = { b_valid: false, s_msg: Strings.coords_not_in_tt, b_x, b_y };

        if (b_x && b_y) {
            o_resp = { b_valid: true };
        }

        return o_resp;

    }



}

module.exports = TabletopM;