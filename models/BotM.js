'use strict'

const Strings = require(`${__dirname}/../config/Strings`);
const Constants = require(`${__dirname}/../config/Constants`)

class BotM {

    constructor(n_x, n_y, s_orientation) {
        this.n_actual_x = parseInt(n_x);
        this.n_actual_y = parseInt(n_y);
        this.s_orientation = s_orientation.toLowerCase();
    }

    // Comprueba los datos del constructor
    static check_constructor_data(n_x, n_y, s_orientation) {
        let o_resp = { b_valid: false, s_msg: Strings.bot_error };

        if (!n_x || n_x == null || !n_y || n_y == null || !s_orientation || s_orientation == null) {
            return o_resp;
        }

        if (isNaN(parseInt(n_x)) || isNaN(parseInt(n_y))) {
            return o_resp;
        }

        if (parseInt(n_x) % 1 != 0 || parseInt(n_y) % 1 != 0) {
            return o_resp;
        }

        if (Constants.A_ORIENTATIONS.indexOf(s_orientation.toLowerCase()) < 0) {
            return o_resp;
        }

        return { b_valid: true };
    }

    // Comprueba las instrucciones dadas
    check_valid_instructions(a_inputs) {
        const o_response = { b_valid: false, s_msg: Strings.instruction_error };

        if (a_inputs.length > 100) {
            return o_response;
        }

        const b_valid = a_inputs.every((s_input) => {
            return Constants.A_INPUTS.indexOf(s_input.toLowerCase()) >= 0;
        })

        if (!b_valid) {
            return o_response;
        }

        return { b_valid: true };
    }

    // Procesa las instrucciones una a una
    process_instructions(a_inputs, o_tabletop) {
        for (let i = 0; i < a_inputs.length; i++) {
            const s_lc_input = a_inputs[i].toLowerCase();

            switch (s_lc_input) {
                case "l":
                    this.determine_orientation(false)
                    break;
                case "r":
                    this.determine_orientation(true)
                    break;
                case "f":
                    const o_operation = this.determine_operation(this.s_orientation);
                    this.execute_operation(o_operation);

                    const o_response = o_tabletop.check_coords_in_tt(this.n_actual_x, this.n_actual_y);

                    // Si no es válida, dejamos las coordenadas anteriores
                    if (!o_response.b_valid) {
                        if (!o_response.b_x) {
                            this.n_actual_x--;
                        } else if (!o_response.b_y) {
                            this.n_actual_y--;
                        }

                        const o_coords = {
                            x: this.n_actual_x,
                            y: this.n_actual_y,
                            orientation: this.s_orientation
                        };

                        const b_marked = o_tabletop.a_marked_coords.some((o_x) => {
                            return o_x.x == o_coords.x && o_x.y == o_coords.y && o_x.orientation == o_coords.orientation;
                        });
                        
                        if (!b_marked) {
                            return { b_valid: false, s_msg: "LOST" };
                        }

                    }

                    break;
                default:
                    break;
            }
        }

        return { b_valid: true };

    }

    // Determina sobre que eje y en que dirección tiene que moverse el robot
    determine_operation(s_orientation) {
        let o_response = {};

        switch (s_orientation.toLowerCase()) {
            case (Constants.S_NORTH):
                o_response = {
                    b_valid: true,
                    b_positive: true,
                    s_axis: "y"
                };
                break;

            case (Constants.S_EAST):
                o_response = {
                    b_valid: true,
                    b_positive: true,
                    s_axis: "x"
                };
                break;

            case (Constants.S_SOUTH):
                o_response = {
                    b_valid: true,
                    b_positive: false,
                    s_axis: "y"
                };
                break;

            case (Constants.S_WEST):
                o_response = {
                    b_valid: true,
                    b_positive: false,
                    s_axis: "x"
                };
                break;

            default:
                o_response = { b_valid: false };
        }

        return o_response;
    }

    // Ejecuta la operación de la instrucción sobre el bot
    execute_operation(o_operation) {
        if (!o_operation.b_valid) {
            return;
        }

        if (o_operation.b_positive) {
            switch (o_operation.s_axis) {
                case "x":
                    this.n_actual_x++;
                    break;
                case "y":
                    this.n_actual_y++;
                    break;
                default:
                    break;
            }
        } else {
            switch (o_operation.s_axis) {
                case "x":
                    this.n_actual_x--;
                    break;
                case "y":
                    this.n_actual_y--;
                    break;
                default:
                    break;
            }
        }

    }

    // Calcula cual es la nueva orientación del bot
    determine_orientation(b_right) {
        const n_index = Constants.A_ORIENTATIONS.indexOf(this.s_orientation);

        let n_next_index = -10;

        if (b_right) {
            n_next_index = n_index + 1;
            if (n_next_index >= Constants.A_ORIENTATIONS.length) {
                n_next_index = 0;
            }
        } else {
            n_next_index = n_index - 1;
            if (n_next_index < 0) {
                n_next_index = Constants.A_ORIENTATIONS.length - 1;
            }
        }
        this.s_orientation = Constants.A_ORIENTATIONS[n_next_index];

    }

}

module.exports = BotM;