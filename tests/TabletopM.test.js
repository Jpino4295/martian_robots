const { expect } = require("chai");

const TabletopM = require(`${__dirname}/../models/TabletopM`);

describe("TabletopM", () => {
    it("Creation must set tabletop data", () => {
        const s_max_x = "15";
        const s_max_y = "30";
        
        const o_tabletop = new TabletopM(s_max_x, s_max_y);

        return new Promise((resolve, reject) => {
            expect(o_tabletop.n_max_x).to.be.equal(parseInt(s_max_x));
            expect(o_tabletop.n_max_y).to.be.equal(parseInt(s_max_y));
            expect(o_tabletop.a_marked_coords).to.be.an("array").to.be.empty;
            resolve();
        });
    });

    it("Coordinates must be valid", () => {
        const s_x = "12";
        const s_y = "45";

        const o_check_coordinates_response = TabletopM.check_coordinates(s_x, s_y);

        return new Promise((resolve, reject) => {
            expect(o_check_coordinates_response.b_valid).to.be.equal(true);
            resolve();
        });
        
    });

    it ("Check coords in tt", () => {
        const o_tabletop = new TabletopM("43", "32");

        const n_x_coord = 25;
        const n_y_coord = 5;

        const o_response = o_tabletop.check_coords_in_tt(n_x_coord, n_y_coord);

        return new Promise((resolve, reject) => {
            expect(o_response.b_valid).to.be.equal(true);
            resolve();
        });
        

    });

    

});