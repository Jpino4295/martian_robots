const { expect } = require("chai");

const InputHelper = require(`${__dirname}/../helpers/InputHelper`);
const Strings = require(`${__dirname}/../config/Strings`);
const Constants = require(`${__dirname}/../config/Constants`);

const a_types = Constants.A_TYPES;

describe("InputHelper", () => {
    it("Must resolve the correct answer", () => {
        
        const o_input_helper = new InputHelper();

        // Random type
        const s_type = a_types[Math.floor(Math.random() * a_types.length)];

        const o_response = o_input_helper.determine_question(s_type);

        return new Promise((resolve, reject) => {
            expect(o_response).to.be.equal(Strings[s_type]);
            o_input_helper.o_rl.close();
            resolve();
        });
        
    });

    it("Must get next type", () => {
        const o_input_helper = new InputHelper();

        const s_type = a_types[Math.floor(Math.random() * (a_types.length - 1))]; 

        const o_response = o_input_helper.check_next_type(s_type);

        const s_next_type = a_types.indexOf(s_type) + 1 >= a_types.length ? a_types[0] : a_types[a_types.indexOf(s_type) + 1];

        return new Promise((resolve, reject) => {
            expect(o_response).to.be.equal(s_next_type);
            o_input_helper.o_rl.close();
            resolve();
        });
        

    });

    it("Must create a tabletop", () => {
        const o_input_helper = new InputHelper();

        const s_input = "4 12";

        const o_response = o_input_helper.process_tabletop(s_input);

        
        return new Promise((resolve, reject) => {
            expect(o_response.b_valid).to.be.equal(true);
            expect(o_input_helper.o_tabletop).to.be.an("object").to.not.be.equal(undefined);
            o_input_helper.o_rl.close();
            resolve();
        });
        
    });

});