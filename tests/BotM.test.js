const { expect } = require("chai");

const BotM = require(`${__dirname}/../models/BotM`);
const Constants = require(`${__dirname}/../config/Constants`);

describe("BotM", () => {
    it("Creation must set bot data", () => {
        const n_x = 5;
        const n_y = 7;
        const s_orientation = "N";

        const o_bot = new BotM(n_x, n_y, s_orientation);

        return new Promise((resolve, reject) => {
            expect(o_bot.n_actual_x).to.be.equal(n_x);
            expect(o_bot.n_actual_y).to.be.equal(n_y);
            expect(o_bot.s_orientation).to.be.equal(s_orientation.toLowerCase());
            resolve();
        })
        

    })

    it("Coords must be valid", () => {
        const n_x = 5;
        const n_y = 73;
        const s_orientation = "N";

        const o_response = BotM.check_constructor_data(n_x, n_y, s_orientation);

        return new Promise((resolve, reject) => {
            expect(o_response.b_valid).to.be.equal(true);
            resolve();
        })
        

    })

    it ("Check valid instructions", () => {
        const o_bot = new BotM(12, 23, "N");

        const a_inputs = ["F", "F", "R", "R", "R", "L", "F", "F", "R", "L", "F", "R"];

        let a_hundred_inputs = [];

        for (let i = 0; i < 150; i++) {
            a_hundred_inputs.push("F");
        }

        const o_response = o_bot.check_valid_instructions(a_inputs);
        const o_response_2 = o_bot.check_valid_instructions(a_hundred_inputs);

        return new Promise((resolve, reject) => {
            expect(o_response.b_valid).to.be.equal(true);
            expect(o_response_2.b_valid).to.be.equal(false);
            resolve();
        })
        

    })

    it ("Determine operator", () => {
        const o_bot = new BotM(12, 23, "N");

        const s_orientation = Constants.S_NORTH;

        const o_response = o_bot.determine_operation(s_orientation);

        return new Promise((resolve, reject) => {
            expect(o_response.b_valid).to.be.equal(true);
            expect(o_response.b_positive).to.be.equal(true);
            expect(o_response.s_axis).to.be.equal("y");
            resolve();
        })
        

    })

    it ("Determine operator", () => {
        const n_x = 12;
        const n_y = 23;
        const s_orientation = "N";

        const o_bot_1 = new BotM(n_x, n_y, s_orientation);
        const o_bot_2 = new BotM(n_x, n_y, s_orientation);
        const o_bot_3 = new BotM(n_x, n_y, s_orientation);
        const o_bot_4 = new BotM(n_x, n_y, s_orientation);

        const s_orientation_1 = Constants.S_NORTH;
        const s_orientation_2 = Constants.S_EAST;
        const s_orientation_3 = Constants.S_SOUTH;
        const s_orientation_4 = Constants.S_WEST;

        const o_operation_1 = o_bot_1.determine_operation(s_orientation_1);
        const o_operation_2 = o_bot_2.determine_operation(s_orientation_2);
        const o_operation_3 = o_bot_3.determine_operation(s_orientation_3);
        const o_operation_4 = o_bot_4.determine_operation(s_orientation_4);

        o_bot_1.execute_operation(o_operation_1);
        o_bot_2.execute_operation(o_operation_2);
        o_bot_3.execute_operation(o_operation_3);
        o_bot_4.execute_operation(o_operation_4);
        
        return new Promise((resolve, reject) => {
            expect(o_bot_1.n_actual_y).to.be.equal(24);
            expect(o_bot_2.n_actual_x).to.be.equal(13);
            expect(o_bot_3.n_actual_y).to.be.equal(22);
            expect(o_bot_4.n_actual_x).to.be.equal(11);
            resolve();
        })
        
    })

    it ("Determine orientation", () => {
        const n_x = 12;
        const n_y = 23;
        const s_orientation_1 = "N";
        const s_orientation_2 = "W";

        const o_bot_1 = new BotM(n_x, n_y, s_orientation_1);
        const o_bot_2 = new BotM(n_x, n_y, s_orientation_2);

        o_bot_1.determine_orientation(true);
        o_bot_2.determine_orientation(false);

        return new Promise((resolve, reject) => {
            expect(o_bot_1.s_orientation).to.be.equal(Constants.S_EAST);
            expect(o_bot_2.s_orientation).to.be.equal(Constants.S_SOUTH);
            resolve();
        })
        

    })
})