# Martian Robots

## Instructions
You have a virtual tabletop where the bots can move.
The first input you have to do is the tabletop size. The tabletop always start in the 0 0 section, and the max dimensions are 50 in each side (left and top). This input accepts 2 non-decimal integers separated by space.

Example: 

    35 28

The second input is the point of deployment of the bot and its orientation. It has to be 2 non-decimal coordinates inside the tabletop and any of the cardinal points (N for north, E for east, S for south, W for west). 

Example:

    15 20 E

The third input is the instructions given to the bot. There are three types of instruction:
 - F - Forward. Make the bot move one section in the direction of its orientation
 - R - Right. Make the bot turn right without moving.
 - L - Left. Make the bot turn left without moving.
All the instructions must be given without any spaces. Also, the bot can't process more than 100 instructions.

Example: 

        FRRFLFLLFFLRRFLLFRF

## Start
In the root directory, use the command `docker-compose run app` to start the game.


## Test
This container will launch a test battery
 - Run the docker-compose command

        docker-compose run test
