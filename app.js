'use strict';

const InputHelper = require(`${__dirname}/helpers/InputHelper`);
const Constants = require(`${__dirname}/config/Constants`);

new InputHelper().request_input(Constants.S_INP_TABLETOP);