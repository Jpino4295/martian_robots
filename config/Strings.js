module.exports = {
    "tabletop": "Insert the tabletop dimensions (x, y) separated by a space\n",
    "bot": "Insert the coordinates and the orientation (x, y, orientation) of the bot, separated by a space\n",
    "instructions": "Insert the instructions for the bot. Insert them without spacing and max 100 instructions at once.\n",
    "tabletop_error": "Please insert 2 coordinates (x, y) separated by space. Example: 5 7\n",
    "coordinates_error": "Please insert 2 non-decimal coordinates between 1 and 50.",
    "bot_error": "Please insert the coordinates and the orientation separated by space. Example: 4 12 N",
    "coords_not_in_tt": "The coordinates given for the bot are outside the tabletop. Please re-enter them.",
    "instruction_error": "The instructions are not correct. Please insert correct options."
};