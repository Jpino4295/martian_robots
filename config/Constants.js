module.exports = {
    S_INP_TABLETOP: "tabletop",
    S_INP_BOT: "bot",
    S_INP_INSTRUCTIONS: "instructions",
    A_TYPES: ["tabletop", "bot", "instructions"],
    A_ORIENTATIONS: ["n", "e", "s", "w"],
    S_NORTH: "n",
    S_EAST: "e",
    S_SOUTH: "s",
    S_WEST: "w",
    A_INPUTS: ["l", "r", "f"],
};