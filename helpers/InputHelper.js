'use strict';
const Readline = require("readline");

const Strings = require(`${__dirname}/../config/Strings`);
const Constants = require(`${__dirname}/../config/Constants`);
const TabletopM = require(`${__dirname}/../models/TabletopM`);
const BotM = require(`${__dirname}/../models/BotM`);

class InputHelper {

    constructor() {
        this.s_last_type = "";

        this.o_tabletop;
        this.o_bot;

        this.o_rl = Readline.createInterface({
            input: process.stdin,
            output: process.stdout
        });

        this.o_rl.on("close", () => {
            console.log("Bye!");
        });
    }



    // Recoge el input, lo procesa y pide el siguiente
    get_input(s_input) {

        const o_process_response = this.process_input(this.s_last_type, s_input);

        if (!o_process_response.b_valid) {
            console.log(o_process_response.s_msg);
            this.request_input(this.s_last_type);
            return;
        }

        const s_next_type = this.check_next_type(this.s_last_type);

        this.request_input(s_next_type);
    }

    // Pide un input
    request_input(s_type) {
        this.s_last_type = s_type;

        const s_question = this.determine_question(s_type);

        this.o_rl.question(s_question, (s_input) => {
            this.get_input(s_input);
        });


    }

    // Determina que tipo es el siguiente
    check_next_type(s_type) {
        let s_next_type = "";

        const n_actual_index = Constants.A_TYPES.indexOf(s_type);

        let n_next_index = n_actual_index + 1 >= Constants.A_TYPES.length ? 0 : n_actual_index + 1;

        s_next_type = Constants.A_TYPES[n_next_index];

        if (this.o_tabletop != undefined && s_next_type == Constants.S_INP_TABLETOP) {
            s_next_type = Constants.A_TYPES[n_next_index + 1];
        }

        return s_next_type;
    }

    // Decide que instrucción aparece en el cli
    determine_question(s_type) {
        let s_response = "";

        switch (s_type) {
            case Constants.S_INP_TABLETOP:
                s_response = Strings[Constants.S_INP_TABLETOP];
                break;

            case Constants.S_INP_BOT:
                s_response = Strings[Constants.S_INP_BOT];
                break;

            case Constants.S_INP_INSTRUCTIONS:
                s_response = Strings[Constants.S_INP_INSTRUCTIONS];
                break;

            default:
                s_response = "error";
                break;
        }

        return s_response;
    }

    // Procesa el input y decide si es válido o no. Cambia lo necesario en el bot / tabletop
    process_input(s_type, s_input) {
        let o_response = { b_valid: false }

        switch (s_type) {
            case Constants.S_INP_TABLETOP:
                o_response = this.process_tabletop(s_input);
                break;
            case Constants.S_INP_BOT:
                o_response = this.process_bot(s_input);
                break;
            case Constants.S_INP_INSTRUCTIONS:
                o_response = this.process_instructions(s_input);
                break;
            default:
                break;

        }

        return o_response;
    }

    // Procesa el input del tablero. Dos coordenadas separadas por espacios
    process_tabletop(s_input) {
        const a_coords = s_input.trim().split(" ");

        if (a_coords.length != 2) {
            return { b_valid: false, s_msg: Strings.tabletop_error };
        }

        const [s_x, s_y] = a_coords;

        const o_check_coordinates_response = TabletopM.check_coordinates(s_x, s_y);

        if (!o_check_coordinates_response.b_valid) {
            return o_check_coordinates_response;
        }

        this.o_tabletop = new TabletopM(s_x.trim(), s_y.trim());

        return { b_valid: true };

    }

    // Crea el bot en unas coordenadas con una orientación
    process_bot(s_input) {
        const [n_x, n_y, s_orientation] = s_input.split(" ");

        const o_bot_response = BotM.check_constructor_data(n_x, n_y, s_orientation)

        // Comprobar que las coordenadas están dentro del tablero
        const o_tt_response = this.o_tabletop.check_coords_in_tt(n_x, n_y);

        if (!o_bot_response.b_valid) {
            return o_bot_response;
        }

        if (!o_tt_response.b_valid) {
            return o_tt_response;
        }

        this.o_bot = new BotM(n_x, n_y, s_orientation);

        return { b_valid: true };


    }

    // Procesa las instrucciones una a una. Cuando avanza comprueba si está dentro del tablero.
    process_instructions(s_input) {
        // Recogemos todos los inputs 
        const a_inputs = s_input.split("");

        const o_valid_instructions = this.o_bot.check_valid_instructions(a_inputs);

        if (!o_valid_instructions.b_valid) {
            return o_valid_instructions;
        }

        const o_response = this.o_bot.process_instructions(a_inputs, this.o_tabletop);

        let s_position = `${this.o_bot.n_actual_x} ${this.o_bot.n_actual_y} ${this.o_bot.s_orientation.toUpperCase()}`;

        if (!o_response.b_valid) {
            s_position += `; ${o_response.s_msg}`;
            this.o_tabletop.a_marked_coords.push({ x: this.o_bot.n_actual_x, y: this.o_bot.n_actual_y, orientation: this.o_bot.s_orientation });
        }

        console.log(`BOT: ${s_position}`);

        return { b_valid: true };
    }

}

module.exports = InputHelper;